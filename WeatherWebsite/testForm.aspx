﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testForm.aspx.cs" Inherits="WeatherWebsite.testForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="/Content/bootstrap.css" rel="stylesheet" />
    <title>Locations table</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:GridView ID="GVLocations" class="table table-striped table-hover" runat="server" EnableViewState="False" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SDSLocations">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="cityName" HeaderText="cityName" SortExpression="cityName" />
                    <asp:BoundField DataField="postcode" HeaderText="postcode" SortExpression="postcode" />
                    <asp:BoundField DataField="countryCode" HeaderText="countryCode" SortExpression="countryCode" />
                    <asp:BoundField DataField="lat" HeaderText="lat" SortExpression="lat" />
                    <asp:BoundField DataField="lon" HeaderText="lon" SortExpression="lon" />
                    <asp:BoundField DataField="units" HeaderText="units" SortExpression="units" />
                </Columns>
            </asp:GridView>

            <asp:SqlDataSource ID="SDSLocations" runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:WeatherConnectionString %>" EnableViewState="False" SelectCommand="SELECT * FROM [Locations]"></asp:SqlDataSource>
            
        </div>
    </form>
</body>
</html>
